ARG PHP_VERSION=7.3

FROM php:${PHP_VERSION}-fpm-alpine

ARG XDEBUG_VERSION=2.7.2
ARG WITH_BLACKFIRE=true

LABEL maintainer="Bruno Paz <oss@brunopaz.net>"

SHELL ["/bin/sh", "-o", "pipefail", "-c"] 

RUN apk add --no-cache \
    make \
    git \
    icu-libs \
    zlib \
    jq \
    nginx \
    runit

ENV APCU_VERSION 5.1.12
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV PHP_CONF_DIR $PHP_INI_DIR/conf.d
# Prevent Symfony Flex from generating a project ID at build time
ENV SYMFONY_SKIP_REGISTRATION=1

RUN set -eux \
    && apk add --no-cache --virtual .build-deps \
    $PHPIZE_DEPS \
    icu-dev \
    zlib-dev \
    libzip-dev \
    && docker-php-ext-install -j"$(nproc)" \
    intl \
    zip \
    pdo_mysql \
    && pecl install \
    apcu-${APCU_VERSION} \
    xdebug-${XDEBUG_VERSION} \
    && docker-php-ext-enable --ini-name 20-apcu.ini apcu \
    && docker-php-ext-enable --ini-name 05-opcache.ini opcache \
    && docker-php-ext-enable --ini-name 00_xdebug.ini xdebug \
    && echo "xdebug.remote_enable=1" >> $PHP_CONF_DIR/xdebug.ini \
    && echo "xdebug.remote_connect_back=0" >> $PHP_CONF_DIR/xdebug.ini \
    && echo "xdebug.remote_autostart=on" >> $PHP_CONF_DIR/xdebug.ini \
    && runDeps="$( \
    scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
    | tr ',' '\n' \
    | sort -u \
    | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
    )" \
    && apk add --no-cache --virtual .api-phpexts-rundeps $runDeps \
    && apk del .build-deps

COPY config/php/php.ini $PHP_INI_DIR/php.ini

# Installs blackfire extension
RUN if [ "$WITH_BLACKFIRE" ]; then \
    set -eux; \
    version=$(php -r "echo PHP_MAJOR_VERSION.PHP_MINOR_VERSION;"); \
    curl -A "Docker" -o /tmp/blackfire-probe.tar.gz -D - -L -s "https://blackfire.io/api/v1/releases/probe/php/alpine/amd64/$version"; \
    mkdir -p /tmp/blackfire; \
    tar zxpf /tmp/blackfire-probe.tar.gz -C /tmp/blackfire; \
    mv /tmp/blackfire/blackfire-*.so "$(php -r "echo ini_get('extension_dir');")/blackfire.so"; \
    printf "extension=blackfire.so\n" > $PHP_CONF_DIR/blackfire.ini; \
    rm -rf /tmp/blackfire /tmp/blackfire-probe.tar.gz; \
    fi ;

# Installs composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer 

# prepare folders
RUN rm /etc/nginx/conf.d/default.conf && \
    mkdir -p /var/www/public && \ 
    mkdir -p /etc/run_once

# Copy supervisor and nginx configuration
COPY config/nginx/nginx.conf /etc/nginx/nginx.conf
COPY config/nginx/symfony.conf /etc/nginx/sites-enabled/default

WORKDIR /var/www

ONBUILD COPY composer.*  ./

# prevent the reinstallation of vendors at every changes in the source code
ONBUILD RUN set -eux; \
    composer install --prefer-dist --no-autoloader --no-scripts --no-progress --no-suggest --no-dev; \
    composer clear-cache

ONBUILD COPY . ./

ONBUILD RUN set -eux; \
    mkdir -p var/cache var/log; \
    composer dump-autoload --classmap-authoritative --no-dev; \
    composer run-script post-install-cmd; \
    chmod +x bin/console; sync

COPY service/nginx /etc/service/nginx
RUN chmod +x /etc/service/nginx/run

COPY service/php-fpm /etc/service/php-fpm
RUN chmod +x /etc/service/php-fpm/run

COPY config/start_services.sh /usr/local/bin/start_services
RUN chmod +x /usr/local/bin/start_services

COPY config/entrypoint.sh /usr/local/bin/docker-app-entrypoint
RUN chmod +x /usr/local/bin/docker-app-entrypoint

ENTRYPOINT ["docker-app-entrypoint"]

CMD ["--server"]
